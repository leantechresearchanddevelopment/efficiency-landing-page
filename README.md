# Efficiency landing page

# Deployed Sites

- Go to [Development Site](https://musing-jang-20fb30.netlify.app/)

This is a *Static site* built with [Gatsby JS](https://github.com/gatsbyjs/gatsby) and [Material-UI](https://material-ui.com/).

The code is organized following an intuitive architecture and follows some of the best practices in *React* & *JavaScript* world, the following features are included in this code baseline:

- **Internationalization**: All texts are localized, so the app support multiple languages.
- **Component Oriented Architecture**: Organized in Pages, Navigation Controls and Common Controls.
- **Built-in controls**: Including Accordion, Autocomplete, Avatar, Checkfield, Datepicker, Multi Select, Modal Dialog, Select, Switch, Table, Textfield and Treeview.
- **Styles**: Supporting SASS, JSS and CSS.

# Environment Setup

### 1. Install Node.js
 - [Download](https://nodejs.org/es/download)

### 2. Install Git
- [Git Client](https://git-scm.com/downloads)
- [Source Tree](https://www.sourcetreeapp.com) (Optional)

### 3. Clone Repository
```shell
git clone https://WilferSalas@bitbucket.org/leantechresearchanddevelopment/efficiency-landing-page.git
```

### 4. Install Visual Studio Code & Extensions
 - [Visual Studio Code](https://code.visualstudio.com/download)

Extensions:
 - [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
 - [EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
 - [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
 - [Jest](https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest)
 - [Icons](https://marketplace.visualstudio.com/items?itemName=robertohuertasm.vscode-icons)
 - [NpmIntellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense)
 - [SortLines](https://marketplace.visualstudio.com/items?itemName=Tyriar.sort-lines)

# Run Project

 1. Install packages: `npm install`
 2. Generate documentation: `npm run doc`
 3. Start project: `npm run dev`
 
 
# Documentation

 - Check out Component's documentation [here](https://bitbucket.org/leantechresearchanddevelopment/efficiency-landing-page/src/master/doc.md).
 
# What's next

Check out the road map of *Efficiency landing page* ...
