// @packages
import CssBaseline from '@material-ui/core/CssBaseline';
import PropTypes from 'prop-types';
import React from 'react';
import { Helmet } from 'react-helmet';
import { Location } from '@reach/router';
import { ThemeProvider } from '@material-ui/core/styles';
import { setConfig } from 'react-hot-loader';

// @scripts
import Footer from '../../src/components/navigation/footer';
import Header from '../../src/components/navigation/header';
import theme from '../../src/theme';
import useSeo from '../../src/hooks/use-seo';

// @styles
import './site.scss';

export default function TopLayout(props) {
  const data = useSeo();
  setConfig({
    showReactDomPatchNotification: false
  });

  const { description, title } = data;

  return (
    <>
      <Helmet htmlAttributes={{ lang: 'en' }}>
        <title>{title}</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
        <meta name="description" content={description} />
        <link
          href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap"
          rel="stylesheet"
        />
      </Helmet>
      <ThemeProvider theme={theme}>
        <Location>
          {locationProps => <Header {...locationProps} {...props} />}
        </Location>
        <CssBaseline />
        {props.children}
        <Footer />
      </ThemeProvider>
    </>
  );
}

TopLayout.propTypes = {
  children: PropTypes.node.isRequired
};
