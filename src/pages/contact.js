// @packages
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { useStaticQuery, graphql } from 'gatsby';

// @scripts
// eslint-disable-next-line prettier/prettier
import ContentPage from '../components/general-purpose/ctrl-content-page';

// @query
const CONTACT = graphql`
  query {
    datoCmsContact {
      title
    }
  }
`;

// @styles
const useStyles = makeStyles({
  button: {
    color: 'white',
    width: 200
  }
});

const Contact = () => {
  const classes = useStyles();
  const {
    datoCmsContact: { title }
  } = useStaticQuery(CONTACT);

  return (
    <ContentPage
      icon="contact"
      node={(
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <TextField fullWidth id="name" label="Name" variant="outlined" />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField fullWidth id="email" label="Email" variant="outlined" />
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              id="message"
              label="Message"
              multiline
              rows={2}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              className={classes.button}
              color="secondary"
              variant="contained"
            >
              Send
            </Button>
          </Grid>
        </Grid>
      )}
      title={title}
    />
  );
};

export default Contact;
