// @packages
import React from 'react';
import loadable from '@loadable/component';

// @scripts
import Faq from '../components/navigation/faq';
import Feature from '../components/navigation/feature';
import Home from '../components/navigation/home';
import Pricing from '../components/navigation/pricing';
import Services from '../components/navigation/service';
import Testimonials from '../components/navigation/testimonial';
import Working from '../components/navigation/working';

const ChatBot = loadable(() => import('../components/general-purpose/ctrl-chatbot'))

const Index = () => (
  <div>
    <ChatBot />
    <Home />
    <Services />
    <Feature />
    <Pricing />
    <Testimonials />
    <Working />
    <Faq />
  </div>
);

export default Index;
