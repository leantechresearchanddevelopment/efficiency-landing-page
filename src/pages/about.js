// @packages
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useStaticQuery, graphql } from 'gatsby';

// @scripts
import ContentPage from '../components/general-purpose/ctrl-content-page';

// @query
const ABOUT = graphql`
  query {
    datoCmsAbout {
      description
      title
    }
  }
`;

// @styles
const useStyles = makeStyles({
  subtitle: {
    lineHeight: 1.75
  }
});

const About = () => {
  const classes = useStyles();
  const {
    datoCmsAbout: { title, description }
  } = useStaticQuery(ABOUT);

  return (
    <ContentPage
      icon="about"
      node={<Typography className={classes.subtitle}>{description}</Typography>}
      title={title}
    />
  );
};

export default About;
