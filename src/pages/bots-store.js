// @packages
import React from 'react';

// @scripts
import Store from '../components/navigation/store';


const BotsStore = () => {
  return (
    <Store />
  );
};

export default BotsStore;
