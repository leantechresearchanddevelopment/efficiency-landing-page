// @packages
import Button from '@material-ui/core/Button';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

// @scripts
import ContentPage from '../components/general-purpose/ctrl-content-page';
import Link from '../components/general-purpose/ctrl-link';

// @styles
const useStyles = makeStyles({
  link: {
    '&:hover': {
      textDecoration: 'none'
    }
  }
});

const About = () => {
  const classes = useStyles();

  return (
    <ContentPage
      icon="404"
      node={(
        <Link className={classes.link} to="/">
          <Button variant="outlined" color="secondary">
            Back to home page
          </Button>
        </Link>
      )}
      title="Page not found"
    />
  );
};

export default About;
