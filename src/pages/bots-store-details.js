// @packages
import React from 'react';

// @scripts
import StoreDetail from '../components/navigation/store-detail';

const BotsStoreDetails = () => {
  return (
    <StoreDetail />
  );
};

export default BotsStoreDetails;
