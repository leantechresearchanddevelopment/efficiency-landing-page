export default [
  { id: 1, description: 'Home', path: 'home' },
  { id: 2, description: 'Service', path: 'service' },
  { id: 3, description: 'Feature', path: 'feature' },
  { id: 4, description: 'Pricing', path: 'pricing' },
  { id: 5, description: 'Testimonial', path: 'testimonial' },
  { id: 6, description: 'Working', path: 'working' },
  { id: 7, description: 'FAQ', path: 'faq' }
];
