export default [
  { id: 1, description: 'About us', url: '/about' },
  { id: 2, description: 'Contact us', url: '/contact' }
];
