// @packages
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import Step from '@material-ui/core/Step';
import StepConnector from '@material-ui/core/StepConnector';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

// @styles
import styles from './styles';

const Connector = withStyles({
    line: {
        backgroundColor: 'white',
        border: 0,
        borderRadius: 1,
        height: 3
    }
})(StepConnector);

const CtrlStepper = ({
    backButtonText,
    className,
    classes,
    completedButtonText,
    id,
    nextButtonText,
    onSubmit,
    stepContent,
    steps,
    visible
}) => {
    const [activeStep, setActiveStep] = useState(0);

    const handleOnNext = () => {
        if (activeStep !== steps.length) {
            setActiveStep(prevActiveStep => prevActiveStep + 1);
        }

        if (activeStep === steps.length - 1) {
            onSubmit();
        }
    };

    const handleOnBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    if (!visible) {
        return null;
    }

    const stepperClass = classNames(
        className,
        classes.stepper
    );

    return (
        <div className={stepperClass} id={id}>
            <Stepper
                className={classes.stepperAction}
                activeStep={activeStep}
                alternativeLabel
                connector={<Connector />}
            >
                {steps.map(step => (
                    <Step key={step}>
                        <StepLabel
                            StepIconProps={{
                                classes: {
                                    root: classes.step,
                                    text: classes.stepText
                                }
                            }}
                        />
                    </Step>
                ))}
            </Stepper>
            {activeStep !== steps.length ? stepContent(activeStep) : stepContent(steps.length - 1)}
            <Box className={classes.buttons}>
                <Button
                    fullWidth
                    className={`
                        ${classes.nextButton}
                        ${activeStep !== 0 ? classes.backButton : null}`
                    }
                    disabled={activeStep === 0}
                    onClick={handleOnBack}
                    variant="outlined"
                >
                    {backButtonText}
                </Button>
                <Button
                    fullWidth
                    className={classes.nextButton}
                    color="secondary"
                    onClick={handleOnNext}
                    variant="contained"
                >
                    {activeStep >= steps.length - 1
                        ? completedButtonText
                        : nextButtonText}
                </Button>
            </Box>
        </div>
    );
};

CtrlStepper.propTypes = {
    backButtonText: PropTypes.string.isRequired,
    className: PropTypes.string,
    classes: PropTypes.object.isRequired,
    completedButtonText: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    nextButtonText: PropTypes.string.isRequired,
    onSubmit: PropTypes.func,
    stepContent: PropTypes.func.isRequired,
    steps: PropTypes.array.isRequired,
    visible: PropTypes.bool
};

CtrlStepper.defaultProps = {
    className: null,
    onSubmit: Function.prototype,
    visible: true
};

export default withStyles(styles)(CtrlStepper);
