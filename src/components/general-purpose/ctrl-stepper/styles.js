import globals from '../../../styles/globals';

export default theme => (Object.assign({}, globals(theme), {
    backButton: {
        borderColor: theme.palette.common.white
    },
    buttons: {
        display: 'flex',
        justifyContent: 'space-around'
    },
    nextButton: {
        margin: '0 10px',
        color: 'white'
    },
    step: {
        '&.MuiStepIcon-root.MuiStepIcon-active': {
            color: theme.palette.common.white,
            border: 'none',
            borderRadius: 0,
            '& .MuiStepIcon-text': {
                fill: theme.palette.secondary.main
            }
        },
        '&.MuiStepIcon-root.MuiStepIcon-completed': {
            color: theme.palette.common.white,
            border: 'none',
            borderRadius: 0
        },
        '&.MuiStepIcon-root': {
            color: 'transparent',
            border: `1px solid ${theme.palette.common.white}`,
            borderRadius: 50
        }
    },
    stepperAction: {
        backgroundColor: 'transparent',
        padding: '0 100px'
    },
}));
