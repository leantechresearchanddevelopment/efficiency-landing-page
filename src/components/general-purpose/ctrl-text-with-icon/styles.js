import globals from '../../../styles/globals';

export default theme => (Object.assign({}, globals(theme), {
    control: {
        color: theme.palette.common.white
    }
}));
