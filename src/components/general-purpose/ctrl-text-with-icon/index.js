// @packages
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

// @styles
import styles from './styles';

const CtrlTextWithIcon = ({
    className,
    classes,
    icon,
    id,
    label,
    visible
}) => {
    if (!visible) {
        return null;
    }

    const controlClass = classNames(
        className,
        classes.control
    );

    return (
        <Grid className={controlClass} container id={id}>
            <Grid
                id={`${id}-icon`}
                item
                xs={1}
            >
                {icon}
            </Grid>
            <Grid
                id={`${id}-label`}
                item
                xs={10}
            >
                <Typography>
                    {label}
                </Typography>
            </Grid>
        </Grid>
    );
};

CtrlTextWithIcon.propTypes = {
    className: PropTypes.string,
    classes: PropTypes.object.isRequired,
    icon: PropTypes.node.isRequired,
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    visible: PropTypes.bool
};

CtrlTextWithIcon.defaultProps = {
    className: null,
    visible: true
};

export default withStyles(styles)(CtrlTextWithIcon);
