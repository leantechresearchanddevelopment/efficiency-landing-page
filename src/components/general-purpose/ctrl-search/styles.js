// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  form: {
    backgroundColor: 'white',
    display: 'flex',
    padding: '12px 12px 12px 0',
    width: '100%'
  },
  iconSearch: {
    width: 25
  },
  input: {
    border: 'none',
    fontSize: '1rem',
    margin: '0 10px',
    '&:focus': {
      outline: 'none'
    }
  }
}));

export default useStyles;
