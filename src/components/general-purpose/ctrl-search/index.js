// @packages
import PropTypes from 'prop-types'
import React from 'react'
import SearchIcon from '@material-ui/icons/Search';

// @styles
import useStyles from './styles';

const CtrlSearch = ({
  classStyle,
  id,
  onSearch,
  value
}) => {
  const classes = useStyles();

  return (
    <div className={classStyle}>
      <form className={classes.form}>
        <SearchIcon className={classes.iconSearch} />
        <input
          className={classes.input}
          id={id}
          name='search'
          onChange={onSearch}
          placeholder='Search'
          type='text'
          value={value}
          autoComplete='off'
        />
      </form>
    </div>
  )
}

CtrlSearch.propTypes = {
  classStyle: PropTypes.string,
  id: PropTypes.string.isRequired,
  onSearch: PropTypes.func,
  value: PropTypes.string
}

CtrlSearch.defaultProps = {
  classStyle: '',
  onSearch: Function.prototype
}

export default CtrlSearch
