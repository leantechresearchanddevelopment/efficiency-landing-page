import globals from '../../../styles/globals';

export default theme => (Object.assign({}, globals(theme), {
    textField: {
        marginBottom: 10
    },
    enabledButton: {
        cursor: 'pointer',
        marginRight: 0
    },
    disabledButton: {
        marginRight: 0
    }
}));
