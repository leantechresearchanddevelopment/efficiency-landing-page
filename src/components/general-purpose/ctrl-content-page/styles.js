// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    alignItems: 'center',
    height: '90vh',
    paddingTop: 30,
    [theme.breakpoints.down('md')]: {
      height: '100vh',
      paddingTop: 100
    }
  },
  containerWithOutTitle: {
    alignItems: 'baseline',
    height: '90vh',
    paddingTop: 60,
    [theme.breakpoints.down('md')]: {
      height: '100vh'
    }
  },
  icon: {
    color: 'orange',
    fontSize: '15rem',
    [theme.breakpoints.down('md')]: {
      fontSize: '12rem'
    }
  },
  iconContainer: {
    textAlign: 'center'
  },
  link: {
    '&:hover': {
      textDecoration: 'none'
    }
  },
  subtitle: {
    lineHeight: 1.75
  },
  title: {
    fontWeight: 600,
    lineHeight: 1.5,
    letterSpacing: '-0.025em',
    marginBottom: 30,
    [theme.breakpoints.down('md')]: {
      fontSize: '2.5rem',
      textAlign: 'center'
    }
  }
}));

export default useStyles;
