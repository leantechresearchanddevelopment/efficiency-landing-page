// @packages
import ComputerIcon from '@material-ui/icons/Computer';
import Container from '@material-ui/core/Container';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import Grid from '@material-ui/core/Grid';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PropTypes from 'prop-types';
import React from 'react';
import Typography from '@material-ui/core/Typography';

// @styles
import useStyles from './styles';

const PageContent = ({ icon, node, title }) => {
  const classes = useStyles();

  const getIcon = (iconName) => {
    switch (iconName) {
      case 'about':
        return <ComputerIcon className={classes.icon} />;
      case 'contact':
        return <MailOutlineIcon className={classes.icon} />;
      case '404':
        return <ErrorOutlineIcon className={classes.icon} />;
      default:
        return null;
    }
  };

  return (
    <Container fixed>
      <Grid
        className={title ? classes.container : `${classes.containerWithOutTitle} ${classes.container}`}
        container
      >
        {icon &&
          <Grid className={classes.iconContainer} item xs={12} md={6}>
            {getIcon(icon)}
          </Grid>
        }
        <Grid item xs={12} md={icon ? 6 : 12}>
          {title &&
            <Typography className={classes.title} variant="h3">
              {title}
            </Typography>
          }
          {node}
        </Grid>
      </Grid>
    </Container>
  );
};

PageContent.propTypes = {
  icon: PropTypes.node,
  node: PropTypes.node.isRequired,
  title: PropTypes.string
};

PageContent.defaultProps = {
  icon: null,
  title: null
};

export default PageContent;
