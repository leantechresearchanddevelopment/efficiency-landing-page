// @packages
import Chatbot from "react-chatbot-kit";
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { loadCSS } from 'fg-loadcss';

// @scripts
import config from "./configs/chatbotConfig";
import MessageParser from "./chatbot/MessageParser";
import ActionProvider from "./chatbot/ActionProvider";


// @styles
import useStyles from './styles';

const ChatBot = ({ className }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  useEffect(() => {
    const node = loadCSS(
      'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
      document.querySelector('#font-awesome-css'),
    );

    return () => {
      node.parentNode.removeChild(node);
    };
  }, []);

  const handleOnOpen = () => {
    setOpen(!open)
  }

  const chatBotClass = classNames(
    className,
    classes.chatBot
  );

  return (
    <ClickAwayListener onClickAway={() => setOpen(false)}>
      <div className={chatBotClass}>
        {open &&
          <Paper elevation={3}>
            <Chatbot
              className={classes.chat}
              config={config}
              messageParser={MessageParser}
              actionProvider={ActionProvider}
            />
          </Paper>
        }
        <Fab color="secondary" onClick={handleOnOpen}>
          <Icon className={`${classes.icon} fas fa-robot`} />
        </Fab>
    </div>
    </ClickAwayListener>
  );
};

ChatBot.propTypes = {
  className: PropTypes.string
};

ChatBot.defaultProps = {
  className: null
};

export default ChatBot;
