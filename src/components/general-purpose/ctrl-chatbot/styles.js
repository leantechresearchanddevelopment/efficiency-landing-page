// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  chat: {
    '&.react-chatbot-kit-chat-btn-send': {
      cursor: 'pointer'
    }
  },
  chatBot: {
    bottom: 20,
    position: 'fixed',
    right: 15,
    textAlign: 'end',
    zIndex: 99999
  },
  icon: {
    color: theme.palette.common.white,
    marginBottom: 5,
    width: '100%'
  }
}));

export default useStyles;
