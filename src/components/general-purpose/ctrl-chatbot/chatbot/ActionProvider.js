const { set } = require("numeral");

class ActionProvider {
  constructor(createChatBotMessage, setStateFunc) {
    this.createChatBotMessage = createChatBotMessage;
    this.setState = setStateFunc;
  }

  setChatBoxState = (message) => {
    this.setState(state => (
      { ...state, messages: [...state.messages, message] }
    ));
  }

  handleOnchatBoxMessage = () => {
    const message = this.createChatBotMessage('Hi!!');

    this.setChatBoxState(message);
  }
}

export default ActionProvider;