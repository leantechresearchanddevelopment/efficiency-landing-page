// @packages
import { createChatBotMessage } from 'react-chatbot-kit';

// @scripts
import theme from '../../../../theme';

// @const
const botName = "Efficiency Bot";

const config = {
  botName: botName,
  lang: "no",
  customStyles: {
    botMessageBox: {
      backgroundColor: theme.palette.primary.main
    },
    chatButton: {
      backgroundColor: theme.palette.secondary.main
    },
  },
  initialMessages: [createChatBotMessage(
    `Hi I'm ${botName}. I’m here to help.`
  )]
}

export default config;