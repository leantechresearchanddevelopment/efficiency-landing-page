// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  button: {
    color: 'white',
    marginTop: 30,
    width: 200
  },
  container: {
    paddingTop: 90,
    textAlign: 'center'
  },
  icon: {
    color: theme.palette.secondary.main,
    fontSize: '6rem'
  },
  image: {
    width: '40vw',
    [theme.breakpoints.up('lg')]: {
      width: '40vw'
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: 30,
      width: '80vw'
    }
  },
  imageContainer: {
    alignSelf: 'center',
    textAlign: 'center'
  },
  info: {
    marginBottom: 50,
    marginTop: 90,
    [theme.breakpoints.down('sm')]: {
      marginBottom: 0,
      marginTop: 20
    }
  },
  infoSubtitle: {
    textAlign: 'initial'
  },
  infoTitle: {
    fontSize: '1.6rem',
    fontWeight: 600,
    padding: 10
  },
  main: {
    height: '80vh',
    [theme.breakpoints.down('sm')]: {
      height: '100vh'
    },
    [theme.breakpoints.down('400')]: {
      height: '130vh'
    }
  },
  rightContent: {
    alignSelf: 'center',
    paddingRight: 65,
    textAlign: 'initial',
    [theme.breakpoints.up('1600')]: {
      padding: '10px 65px',
      paddingRight: 330
    },
    [theme.breakpoints.down('sm')]: {
      padding: '10px 25px'
    }
  },
  rightContentTitle: {
    fontWeight: 600,
    lineHeight: 1.5,
    letterSpacing: '-0.025em',
    marginBottom: 30,
    [theme.breakpoints.down('md')]: {
      fontSize: '2.9rem'
    }
  },
  subtitle: {
    fontSize: '1.6rem',
    fontWeight: 600
  },
  title: {
    color: theme.palette.secondary.main,
    fontWeight: 600
  }
}));

export default useStyles;
