// @packages
import AssignmentIcon from '@material-ui/icons/Assignment';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Grid from '@material-ui/core/Grid';
import Img from 'gatsby-image';
import React, { useState } from 'react';
import SpeedIcon from '@material-ui/icons/Speed';
import Typography from '@material-ui/core/Typography';
import { InView } from 'react-intersection-observer';
import { motion } from "framer-motion";
import { useStaticQuery, graphql } from 'gatsby';

// @styles
import useStyles from './styles';

// @query
const SERVICE = graphql`
  query {
    datoCmsService {
      button
      description
      descriptionTwo
      image {
        fluid(maxWidth: 2000) {
          ...GatsbyDatoCmsFluid
        }
      }
      items
      title
      titleTwo
    }
  }
`;

const Home = () => {
  const classes = useStyles();
  const [inView, setInView] = useState(false);
  const {
    datoCmsService: {
      button,
      description,
      descriptionTwo,
      image,
      items,
      title,
      titleTwo
    }
  } = useStaticQuery(SERVICE);

  const getIcon = (number) => {
    switch (number) {
      case 'FavoriteIcon':
        return <FavoriteIcon className={classes.icon} />;
      case 'AssignmentIcon':
        return <AssignmentIcon className={classes.icon} />;
      case 'SpeedIcon':
        return <SpeedIcon className={classes.icon} />;
      default:
        return null;
    }
  };

  const handleOnInView = (inView) => {
    if (inView) {
      setInView(true)
    }
  };

  return (
    <div>
      <Container fixed>
        <Grid className={classes.container} container id="service" spacing={3}>
          <Grid item xs={12}>
            <Typography className={classes.title}>{title}</Typography>
            <Typography className={classes.subtitle}>{description}</Typography>
          </Grid>
          {JSON.parse(items).map((item) => (
            <Grid className={classes.info} item key={item.id} md={4} sm={12}>
              <motion.div
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
              >
                {getIcon(item.icon)}
              </motion.div>
              <Typography className={classes.infoTitle}>
                {item.title}
              </Typography>
              <Typography className={classes.infoSubtitle}>
                {item.description}
              </Typography>
            </Grid>
          ))}
        </Grid>
      </Container>
      <Grid className={classes.main} container>
        <Grid className={classes.imageContainer} item xs={12} md={6}>
          <InView as="div" onChange={(inView, entry) => handleOnInView(inView)}>
            {inView &&
                <motion.div
                  animate={{  opacity: 1, x: [-500, 0] }}
                  transition={{ duration: 1.5 }}
                >
                  <Img className={classes.image} fluid={image.fluid} />
                </motion.div>
            }
          </InView>
        </Grid>
        <Grid className={classes.rightContent} item xs={12} md={6}>
          <Typography className={classes.rightContentTitle} variant="h3">
            {titleTwo}
          </Typography>
          <Typography>{descriptionTwo}</Typography>
          <Button
            className={`${classes.button} ${classes.buttonFreeTrial}`}
            color="secondary"
            variant="contained"
          >
            {button}
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

export default Home;
