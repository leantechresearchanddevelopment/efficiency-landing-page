// @packages
import AppBar from '@material-ui/core/AppBar';
import Container from '@material-ui/core/Container';
import Link from '../../general-purpose/ctrl-link';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import { globalHistory } from '@reach/router';

// @scripts
import Menu from './menu';

// @styles
import useStyles from './styles';

// @images
import Logo from '../../../assets/images/logo.png';

const Header = (props) => {
  const classes = useStyles();

  const { window } = props;

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined
  });

  const isHomePage = props.location.pathname === '/';

  return (
    <AppBar className={!trigger ? classes.appBar : null} color="inherit">
      <Container className={classes.container} fixed>
        <Link className={classes.link} to="/">
          <div className={classes.logo}>
            <img alt="logo" className={classes.img} src={Logo} />
            <Typography variant="h6" className={classes.title}>
              Efficiency
            </Typography>
          </div>
        </Link>
        {isHomePage && <Menu path={globalHistory.location.hash} />}
      </Container>
    </AppBar>
  );
};

export default Header;
