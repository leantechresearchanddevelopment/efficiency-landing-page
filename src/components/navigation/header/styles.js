import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  appBar: {
    boxShadow: 'none',
    backgroundColor: 'transparent',
    padding: '10px 0'
  },
  container: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
    padding: 0,
    position: 'relative'
  },
  img: {
    height: 30,
    marginBottom: 5,
    padding: '0 10px 4px 10px'
  },
  link: {
    color: theme.palette.common.black,
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'none'
    }
  },
  logo: {
    alignItems: 'center',
    cursor: 'pointer',
    display: 'flex',
    height: 60,
    padding: 10
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  root: {
    flexGrow: 1
  },
  title: {
    flexGrow: 1
  }
}));

export default useStyles;
