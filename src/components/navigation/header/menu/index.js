// @packages
import Button from '@material-ui/core/Button';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import React, { useState } from 'react';
import StoreIcon from '@material-ui/icons/Store';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import MaterialLink from '../../../general-purpose/ctrl-link';
import { Link } from 'react-scroll';

// @styles
import useStyles from './styles';

// @constants
import menuItems from '../../../../config/constants/menu';

const Menu = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const matches = useMediaQuery((theme) => theme.breakpoints.up('md'));

  const HandleOnClick = (open) => (event) => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setOpen(open);
  };

  return (
    <>
      <div className={classes.menu}>
        {matches &&
          menuItems.map((menu) => (
            <Link
              activeClass="active"
              className={classes.link}
              duration={1000}
              key={menu.id}
              smooth={true}
              spy={true}
              to={menu.path}
            >
              {menu.description}
            </Link>
          ))}
          <Button
            color="secondary"
            endIcon={<StoreIcon />}
            variant="outlined"
          >
            <MaterialLink className={classes.materialLink} to='/bots-store'>
              Bots store
            </MaterialLink>
          </Button>
        {!matches && (
          <IconButton onClick={HandleOnClick(true)} aria-label="menu-button">
            <MenuIcon className={classes.icon} />
          </IconButton>
        )}
      </div>
      <Drawer
        anchor="right"
        className={classes.drawer}
        classes={{ paperAnchorRight: classes.drawer }}
        onClose={HandleOnClick(false)}
        open={open}
      >
        {menuItems.map((menu) => (
          <Link
            activeClass="active"
            className={`${classes.link} ${classes.linkMenu}`}
            duration={1000}
            key={menu.id}
            smooth={true}
            spy={true}
            to={menu.path}
          >
            {menu.description}
          </Link>
        ))}
      </Drawer>
    </>
  );
};

export default Menu;
