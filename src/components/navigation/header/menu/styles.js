import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  link: {
    color: theme.palette.common.black,
    fontSize: '0.95rem',
    fontWeight: 400,
    margin: '0 10px',
    textAlign: 'center',
    '&:hover': {
      backgroundColor: 'transparent',
      color: theme.palette.secondary.main,
      cursor: 'pointer',
      textDecoration: 'none'
    }
  },
  linkMenu: {
    margin: '5px 0'
  },
  drawer: {
    paddingTop: 53,
    width: 200
  },
  icon: {
    color: theme.palette.secondary.main
  },
  materialLink: {
    color: theme.palette.secondary.main,
    '&:hover': {
      color: theme.palette.secondary.main,
      textDecoration: 'none'
    }
  },
  menu: {
    position: 'absolute',
    textAlign: 'center',
    width: '100%',
    [theme.breakpoints.down('sm')]: {
      position: 'inherit',
      width: 'auto'
    }
  }
}));

export default useStyles;
