// @packages
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CheckIcon from '@material-ui/icons/Check';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import React, { useState } from 'react';
import Typography from '@material-ui/core/Typography';
import numeral from 'numeral';
import { useStaticQuery, graphql } from 'gatsby';
import theme from '../../../theme';

// @styles
import useStyles from './styles';

// @query
const PRICING = graphql`
  query {
    datoCmsPricing {
      annualItems
      buttonAnnual
      buttonMonthly
      description
      monthyItems
      title
    }
  }
`;

const Pricing = () => {
  const classes = useStyles();
  const [plan, setPlan] = useState('monthly');
  const {
    datoCmsPricing: {
      annualItems,
      buttonAnnual,
      buttonMonthly,
      description,
      monthyItems,
      title
    }
  } = useStaticQuery(PRICING);

  return (
    <Container fixed>
      <Grid className={classes.container} container id="pricing">
        <Grid item xs={12}>
          <Typography className={classes.title}>{title}</Typography>
          <Typography className={classes.subtitle}>{description}</Typography>
        </Grid>
        <Grid item xs={12}>
          <Button
            className={classes.buttonOption}
            color={plan === 'monthly' ? 'secondary' : 'primary'}
            onClick={() => setPlan('monthly')}
            variant="outlined"
          >
            {buttonMonthly}
          </Button>
          <Button
            className={classes.buttonOption}
            color={plan === 'annual' ? 'secondary' : 'primary'}
            onClick={() => setPlan('annual')}
            variant="outlined"
          >
            {buttonAnnual}
          </Button>
        </Grid>
        {(plan === 'monthly'
          ? JSON.parse(monthyItems)
          : JSON.parse(annualItems)).map((item) => (
          <Grid
            className={classes.cardContainer}
            item
            xs={12}
            md={4}
            key={item.id}
          >
            <Card className={classes.root} variant="outlined">
              <CardContent className={classes.cardContent}>
                <Typography className={classes.cardTitle} gutterBottom>
                  {item.title}
                </Typography>
                <Typography
                  className={classes.cardDescription}
                  color="textSecondary"
                >
                  {item.description}
                </Typography>
                <Typography className={classes.cardValue}>
                  {numeral(item.price).format('($0.00)')}
                </Typography>
                <Typography
                  className={classes.cardDescription}
                  color="textSecondary"
                >
                  {item.price_description}
                </Typography>
              </CardContent>
              <CardActions className={classes.cardAction}>
                <Button
                  className={classes.cardButton}
                  color="secondary"
                  style={
                    item.id === 0
                      ? { color: theme.palette.secondary.main }
                      : null
                  }
                  variant={item.id !== 0 ? 'contained' : 'outlined'}
                >
                  {item.button_text}
                </Button>
              </CardActions>
              <CardContent className={classes.cardContent}>
                {item.benefits.map(({ id, benefit }) => (
                  <div className={classes.cardOptions} key={id}>
                    <CheckIcon className={classes.cardIcon} />
                    <Typography color="textSecondary" gutterBottom>
                      {benefit}
                    </Typography>
                  </div>
                ))}
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Pricing;
