// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  buttonOption: {
    marginBottom: 80,
    width: 200,
    [theme.breakpoints.down('sm')]: {
      width: 'auto'
    }
  },
  cardAction: {
    justifyContent: 'center'
  },
  cardButton: {
    color: theme.palette.common.white
  },
  cardContainer: {
    padding: '0 40px',
    [theme.breakpoints.down('sm')]: {
      marginBottom: 10
    },
    [theme.breakpoints.down('400')]: {
      padding: '0 20px'
    }
  },
  cardContent: {
    padding: '36px 16px'
  },
  cardDescription: {
    fontSize: '0.9rem',
    lineHeight: 1.75
  },
  cardIcon: {
    color: 'green',
    fontSize: '1.2rem',
    margin: '2px 10px'
  },
  cardOptions: {
    display: 'flex'
  },
  cardTitle: {
    fontSize: '1.3rem',
    fontWeight: 500
  },
  cardValue: {
    fontSize: '2.5rem',
    fontWeight: 400,
    marginTop: 30
  },
  container: {
    justifyContent: 'space-between',
    padding: '100px 0',
    textAlign: 'center'
  },
  options: {
    justifyContent: 'space-between'
  },
  subtitle: {
    fontSize: '1.6rem',
    fontWeight: 600,
    marginBottom: 50
  },
  title: {
    color: theme.palette.secondary.main,
    fontWeight: 600
  }
}));

export default useStyles;
