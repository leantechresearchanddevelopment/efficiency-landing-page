// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  button: {
    width: 150,
    [theme.breakpoints.down('400')]: {
      width: 'auto'
    }
  },
  buttonContainer: {
    marginTop: 30,
    width: '100%'
  },
  buttonFreeTrial: {
    color: theme.palette.common.white,
    marginRight: 10
  },
  home: {
    height: '100vh',
    paddingTop: 100,
    position: 'absolute',
    right: '0%',
    top: 0,
    [theme.breakpoints.down('sm')]: {
      height: '80vh',
      marginLeft: '-55px',
      paddingTop: 80,
      position: 'initial'
    },
    [theme.breakpoints.down('400')]: {
      height: '110vh'
    }
  },
  homeAnimation: {
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  image: {
    width: '49.5vw',
    marginTop: '-100px',
    [theme.breakpoints.down('lg')]: {
      width: '49.3vw'
    },
    [theme.breakpoints.down('md')]: {
      width: '50vw'
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  leftContent: {
    display: 'flex',
    alignItems: 'center'
  },
  leftContentDiv: {
    paddingLeft: 65,
    position: 'relative',
    width: '90%',
    [theme.breakpoints.up('1600')]: {
      paddingLeft: 320,
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  leftContentSubtitle: {
    lineHeight: 1.75
  },
  leftContentTitle: {
    fontWeight: 600,
    lineHeight: 1.5,
    letterSpacing: '-0.025em',
    marginBottom: 30,
    [theme.breakpoints.down('md')]: {
      fontSize: '2.9rem'
    }
  }
}));

export default useStyles;
