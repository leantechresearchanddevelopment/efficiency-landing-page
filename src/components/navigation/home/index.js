// @packages
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Img from 'gatsby-image';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { motion } from "framer-motion";
import { useStaticQuery, graphql } from 'gatsby';

// @scripts
import HomeAnimation from './background-animation';

// @styles
import useStyles from './styles';

// @query
const HOME = graphql`
  query {
    datoCmsHome {
      buttonExploreMore
      buttonFreeTrial
      description
      image {
        fluid(maxWidth: 1200) {
          ...GatsbyDatoCmsFluid
        }
      }
      title
    }
  }
`;

const Home = () => {
  const classes = useStyles();
  const {
    datoCmsHome: {
      buttonExploreMore,
      buttonFreeTrial,
      description,
      image,
      title
    }
  } = useStaticQuery(HOME);

  return (
    <div>
      <HomeAnimation />
      <Container fixed>
        <Grid className={classes.home} container id="home">
          <Grid className={classes.leftContent} item xs={12} md={6}>
            <div className={classes.leftContentDiv}>
              <Typography className={classes.leftContentTitle} variant="h3">
                {title}
              </Typography>
              <Typography className={classes.leftContentSubtitle}>
                {description}
              </Typography>
              <div className={classes.buttonContainer}>
                <Button
                  className={`${classes.button} ${classes.buttonFreeTrial}`}
                  color="secondary"
                  variant="contained"
                >
                  {buttonFreeTrial}
                </Button>
                <Button
                  className={classes.button}
                  color="secondary"
                  variant="outlined"
                >
                  {buttonExploreMore}
                </Button>
              </div>
            </div>
          </Grid>
          <Grid item xs={6}>
            <motion.div
              animate={{ scale: [0.5, 1] }}
              transition={{ duration: 0.5 }}
            >
              <Img className={classes.image} fluid={image.fluid} />
            </motion.div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Home;
