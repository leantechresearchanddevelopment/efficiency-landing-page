// @packages
import Brightness1Icon from '@material-ui/icons/Brightness1';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Img from 'gatsby-image';
import React, { useState } from 'react';
import Timeline from '@material-ui/lab/Timeline';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import Typography from '@material-ui/core/Typography';
import numeral from 'numeral';
import { InView } from 'react-intersection-observer';
import { motion } from "framer-motion";
import { useStaticQuery, graphql } from 'gatsby';

// @styles
import useStyles from './styles';

// @query
const WORKING = graphql`
  query {
    datoCmsWorking {
      button
      description
      image {
        fluid(maxWidth: 1000) {
          ...GatsbyDatoCmsFluid
        }
      }
      items
      title
    }
  }
`;

const Working = () => {
  const classes = useStyles();
  const [inView, setInView] = useState(false);
  const {
    datoCmsWorking: { button, description, image, items, title }
  } = useStaticQuery(WORKING);

  const handleOnInView = (inView) => {
    if (inView) {
      setInView(true)
    }
  };

  return (
    <Container id="working" fixed>
      <Grid className={classes.container} container id="working">
        <Grid item xs={12}>
          <Typography className={classes.title}>{title}</Typography>
          <Typography className={classes.subtitle}>{description}</Typography>
        </Grid>
        <Grid className={classes.imageContainer} item xs={12} md={6}>
          <InView as="div" onChange={(inView, entry) => handleOnInView(inView)}>
            {inView &&
              <motion.div
                animate={{ opacity: [0, 1], y: [200, 0] }}
                transition={{ duration: 1.5 }}
              >
                <Img className={classes.image} fluid={image.fluid} />
              </motion.div>
            }
          </InView>
        </Grid>
        <Grid item xs={12} md={6}>
          <Timeline className={classes.timeline}>
            {JSON.parse(items).map((item, index, { length }) => (
              <TimelineItem
                classes={{ missingOppositeContent: classes.timelineItem }}
                key={item.id}
              >
                <TimelineSeparator>
                  <TimelineDot variant="outlined">
                    <Brightness1Icon fontSize="small" />
                  </TimelineDot>
                  {index + 1 !== length && <TimelineConnector />}
                </TimelineSeparator>
                <TimelineContent className={classes.timelineContent}>
                  <Grid container>
                    <Grid
                      className={classes.timelineNumberContainer}
                      item
                      xs={2}
                    >
                      <Typography className={classes.timelineNumber}>
                        {numeral(item.id + 1).format('00')}
                      </Typography>
                    </Grid>
                    <Grid item xs={10}>
                      <Typography className={classes.timelineTitle}>
                        {item.title}
                      </Typography>
                      <Typography className={classes.timelineSubtitle}>
                        {item.description}
                      </Typography>
                    </Grid>
                  </Grid>
                </TimelineContent>
              </TimelineItem>
            ))}
          </Timeline>
        </Grid>
        <Grid item xs={12}>
          <Button
            className={classes.button}
            color="secondary"
            variant="contained"
          >
            {button}
          </Button>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Working;
