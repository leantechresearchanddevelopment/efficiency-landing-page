// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  button: {
    color: theme.palette.common.white,
    width: 200
  },
  container: {
    paddingTop: 100,
    textAlign: 'center'
  },
  image: {
    [theme.breakpoints.down('md')]: {
      width: '50vw'
    }
  },
  imageContainer: {
    [theme.breakpoints.down('md')]: {
      display: 'flex',
      justifyContent: 'center'
    }
  },
  subtitle: {
    fontSize: '1.6rem',
    fontWeight: 600,
    marginBottom: 50
  },
  timeline: {
    marginTop: 60,
    textAlign: 'initial'
  },
  timelineContent: {
    marginTop: '-30px',
    paddingBottom: 160,
    [theme.breakpoints.down('400')]: {
      marginTop: '-25px'
    }
  },
  timelineContentLastItem: {
    marginTop: '-40px'
  },
  timelineItem: {
    '&:before': {
      display: 'none'
    }
  },
  timelineNumber: {
    color: `${theme.palette.common.black}33`,
    fontSize: '3.9rem',
    [theme.breakpoints.down('md')]: {
      fontSize: '2.5rem'
    },
    [theme.breakpoints.down('400')]: {
      fontSize: '1.9rem'
    }
  },
  timelineNumberContainer: {
    alignSelf: 'center',
    paddingRight: 10,
    textAlign: 'center'
  },
  timelineSubtitle: {
    [theme.breakpoints.down('400')]: {
      fontSize: '0.8rem',
    }
  },
  timelineTitle: {
    fontSize: '1.4rem',
    fontWeight: 500,
    marginBottom: 10,
    [theme.breakpoints.down('md')]: {
      fontSize: '1rem',
      marginBottom: 5
    }
  },
  title: {
    color: theme.palette.secondary.main,
    fontWeight: 600
  }
}));

export default useStyles;
