// @packages
import Button from '@material-ui/core/Button';
import Carousel from 'react-material-ui-carousel';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import Img from 'gatsby-image';
import Paper from '@material-ui/core/Paper';
import React, { useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import { loadCSS } from 'fg-loadcss';
import { useStaticQuery, graphql } from 'gatsby';

// @styles
import useStyles from './styles';

// @query
const TESTIMONIAL = graphql`
  query {
    datoCmsTestimonial {
      button
      description
      image {
        fluid(maxWidth: 1200) {
          ...GatsbyDatoCmsFluid
        }
      }
      items
      title
      titleTwo
    }
  }
`;

const Testimonials = () => {
  const classes = useStyles();
  const {
    datoCmsTestimonial: {
      button,
      description,
      image,
      items,
      title,
      titleTwo
    }
  } = useStaticQuery(TESTIMONIAL);

  useEffect(() => {
    const node = loadCSS(
      'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
      document.querySelector('#font-awesome-css')
    );

    return () => {
      node.parentNode.removeChild(node);
    };
  }, []);

  return (
    <div>
      <Container fixed>
        <div className={classes.container} id="testimonial">
          <Typography className={classes.title}>{title}</Typography>
          <Carousel>
            {JSON.parse(items).map((item) => (
              <Paper className={classes.paper} elevation={0} key={item.id}>
                <Grid container>
                  <Grid className={classes.commentSection} item xs={7} md={9}>
                    <Icon
                      className={`${classes.icon} fas fa-quote-left`}
                      color="primary"
                    />
                    <Typography className={classes.comment}>
                      {item.comment}
                    </Typography>
                    <div>
                      <Typography className={classes.author} display="inline">
                        {item.author}
                      </Typography>
                      <Typography display="inline">{item.company}</Typography>
                    </div>
                  </Grid>
                  <Grid item xs={5} md={3}>
                    <img
                      className={classes.imagePerson}
                      src={item.image}
                      alt={item.image}
                    />
                  </Grid>
                </Grid>
              </Paper>
            ))}
          </Carousel>
        </div>
      </Container>
      <Grid className={classes.partners} container>
        <Grid className={classes.imageContainer} item xs={12} md={6}>
          <Img className={classes.image} fluid={image.fluid} />
        </Grid>
        <Grid className={classes.rightContent} item xs={12} md={6}>
          <Typography className={classes.rightContentTitle} variant="h3">
            {titleTwo}
          </Typography>
          <Typography>{description}</Typography>
          <Button
            className={`${classes.button} ${classes.buttonFreeTrial}`}
            color="secondary"
            variant="contained"
          >
            {button}
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

export default Testimonials;
