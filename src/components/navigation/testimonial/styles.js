// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  author: {
    fontWeight: 500,
    marginRight: 10
  },
  button: {
    color: 'white',
    marginTop: 30,
    width: 200
  },
  container: {
    paddingTop: 100,
    paddingLeft: 100,
    [theme.breakpoints.down('md')]: {
      margin: 0,
      paddingLeft: 10,
      paddingTop: 0
    }
  },
  comment: {
    fontSize: 30,
    marginBottom: 25,
    marginRight: 30,
    textIndent: 27,
    [theme.breakpoints.down('sm')]: {
      fontSize: '1rem'
    }
  },
  commentSection: {
    alignSelf: 'center'
  },
  icon: {
    position: 'absolute',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.2rem'
    }
  },
  image: {
    width: '35vw',
    [theme.breakpoints.down('md')]: {
      width: '80vw'
    }
  },
  imagePerson: {
    borderRadius: '50%',
    width: 200,
    [theme.breakpoints.down('sm')]: {
      width: 120
    }
  },
  imageContainer: {
    alignSelf: 'center',
    display: 'flex',
    justifyContent: 'flex-end',
    paddingRight: 50
  },
  paper: {
    margin: '20px 0'
  },
  partners: {
    backgroundColor: `${theme.palette.secondary.main}0D`,
    height: '70vh',
    marginTop: 70,
    [theme.breakpoints.down('lg')]: {
      height: '100vh'
    },
    [theme.breakpoints.down('400')]: {
      height: '120vh'
    }
  },
  rightContent: {
    alignSelf: 'center',
    paddingRight: 65,
    textAlign: 'initial',
    [theme.breakpoints.up('1600')]: {
      paddingRight: 330
    },
    [theme.breakpoints.down('md')]: {
      padding: '10px 25px'
    }
  },
  rightContentTitle: {
    fontWeight: 600,
    lineHeight: 1.5,
    letterSpacing: '-0.025em',
    marginBottom: 30,
    [theme.breakpoints.down('md')]: {
      fontSize: '2.9rem'
    }
  },
  subtitle: {
    fontSize: '1.6rem',
    fontWeight: 600
  },
  title: {
    color: theme.palette.secondary.main,
    fontWeight: 600
  }
}));

export default useStyles;
