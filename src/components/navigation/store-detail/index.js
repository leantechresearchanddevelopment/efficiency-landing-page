// @packages
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Img from 'gatsby-image';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MuiAccordion from '@material-ui/core/Accordion';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import numeral from 'numeral';
import { graphql } from 'gatsby';
import { motion } from "framer-motion";
import { withStyles } from '@material-ui/core/styles';

// @styles
import useStyles from './styles';

// @query
export const query = graphql`
  query($slug: String!) {
    allDatoCmsBotsStore(filter: { slug: { eq: $slug } }) {
      nodes {
        benefits {
          description
        }
        description
        image {
          fluid(maxWidth: 300) {
            ...GatsbyDatoCmsFluid
          }
        }
        price
        subtitle
        tasks {
          description
        }
        title
      }
    }
  }
`;

const Accordion = withStyles({
  root: {
    '&$expanded': {
      margin: 'auto'
    }
  },
  expanded: {}
})(MuiAccordion);

const StoreDetail = ({ data }) => {
  const classes = useStyles();

  if (!data) {
    return null;
  }

  const { allDatoCmsBotsStore: {
    nodes: { 0: {
      benefits,
      description,
      image,
      price,
      tasks,
      title
    }}
  }} = data;

  return (
    <Container style={{ overflow: "hidden", minHeight: 'calc(100vh - 70px)' }}>
      <Grid className={classes.store} container spacing={3}>
        <Grid container className={classes.imgContainer} item md={12} lg={6}>
          <Grid item xs={12}>
            <motion.div
              animate={{ scale: [0.5, 1] }}
              transition={{ duration: 0.5 }}
            >
              <Img className={classes.img} fluid={image.fluid} />
            </motion.div>
          </Grid>
        </Grid>
        <Grid item md={12} lg={6}>
          <Typography variant="h4">
            {title}
          </Typography>
          <Typography gutterBottom>
            {numeral(price).format('($0.00)')}
          </Typography>
          <Divider className={classes.divider} />
          <Typography>
            {description}
          </Typography>
          <Button
            className={classes.button}
            variant="contained"
            color="secondary"
          >
            Buy
          </Button>
        </Grid>
        <Grid item md={12} lg={6} />
        <Grid item md={12} lg={6}>
          <Divider />
          <Accordion className={classes.accordion}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>Benefits</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <List className={classes.list}>
                {benefits.map((item, index) => (
                  <ListItem button key={index}>
                    <ListItemText primary={item.description} />
                  </ListItem>
                ))}
              </List>
            </AccordionDetails>
          </Accordion>
          <Divider />
          <Accordion className={classes.accordion}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>Tasks</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <List>
                  {tasks.map((item, index) => (
                    <ListItem button key={index}>
                      <ListItemText primary={item.description} />
                    </ListItem>
                  ))}
                </List>
              </AccordionDetails>
            </Accordion>
          <Divider />
        </Grid>
      </Grid>
    </Container>
  );
};

export default StoreDetail;
