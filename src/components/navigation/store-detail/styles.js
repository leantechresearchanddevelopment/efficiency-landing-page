// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  accordion: {
    backgroundColor: `${theme.palette.secondary.main}01`,
    boxShadow: 'none',
    textAlign: 'initial'
  },
  button: {
    color: theme.palette.common.white,
    marginTop: 110,
    width: 170
  },
  divider: {
    margin: '20px 0'
  },
  imgContainer: {
    alignSelf: 'center',
    display: 'flex',
    justifyContent: 'center'
  },
  list: {
    width: '100%'
  },
  store: {
    marginBottom: 20,
    marginTop: 90
  }
}));

export default useStyles;
