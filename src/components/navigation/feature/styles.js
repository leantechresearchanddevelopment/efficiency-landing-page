// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: 100,
    textAlign: 'center'
  },
  icon: {
    color: theme.palette.secondary.main,
    fontSize: '4rem'
  },
  iconContainer: {
    textAlign: 'center'
  },
  itemFeature: {
    margin: '30px 0'
  },
  itemSubtitle: {
    boxSizing: 'border-box',
    color: 'rgba(52, 61, 76, 0.8)',
    fontSize: 'inherit',
    lineHeight: 1.75,
    marginBottom: '1rem',
    marginTop: 0,
    textAlign: 'initial'
  },
  itemTitle: {
    boxSizing: 'border-box',
    color: 'rgb(15, 33, 55)',
    fontSize: '1.2rem',
    fontWeight: 400,
    letterSpacing: '-0.02em',
    lineHeight: 1.5,
    marginBottom: 10,
    marginTop: 0,
    textAlign: 'initial',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1rem',
    }
  },
  subtitle: {
    fontSize: '1.6rem',
    fontWeight: 600,
    marginBottom: 50
  },
  title: {
    color: theme.palette.secondary.main,
    fontWeight: 600
  }
}));

export default useStyles;
