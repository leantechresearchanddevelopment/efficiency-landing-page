// @packages
import ChatIcon from '@material-ui/icons/Chat';
import CloudIcon from '@material-ui/icons/Cloud';
import CodeIcon from '@material-ui/icons/Code';
import Container from '@material-ui/core/Container';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import Grid from '@material-ui/core/Grid';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import React from 'react';
import StarIcon from '@material-ui/icons/Star';
import Typography from '@material-ui/core/Typography';
import { motion } from "framer-motion";
import { useStaticQuery, graphql } from 'gatsby';

// @styles
import useStyles from './styles';

// @query
const FEATURES = graphql`
  query {
    datoCmsFeature {
      description
      items
      title
    }
  }
`;

const Feature = () => {
  const classes = useStyles();
  const {
    datoCmsFeature: { description, items, title }
  } = useStaticQuery(FEATURES);

  const getIcon = (iconName) => {
    switch (iconName) {
      case 'CodeIcon':
        return <CodeIcon className={classes.icon} />;
      case 'StarIcon':
        return <StarIcon className={classes.icon} />;
      case 'CloudIcon':
        return <CloudIcon className={classes.icon} />;
      case 'InsertEmoticonIcon':
        return <InsertEmoticonIcon className={classes.icon} />;
      case 'EmojiObjectsIcon':
        return <EmojiObjectsIcon className={classes.icon} />;
      case 'ChatIcon':
        return <ChatIcon className={classes.icon} />;
      default:
        return null;
    }
  };

  return (
    <Container fixed>
      <Grid className={classes.container} container id="feature">
        <Grid item xs={12}>
          <Typography className={classes.title}>{title}</Typography>
          <Typography className={classes.subtitle}>{description}</Typography>
        </Grid>
        {JSON.parse(items).map((item) => (
          <Grid
            className={classes.itemFeature}
            container
            item
            key={item.id}
            md={4}
            xs={12}
          >
            <Grid className={classes.iconContainer} item xs={3}>
              <motion.div
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
              >
                {getIcon(item.icon)}
              </motion.div>
            </Grid>
            <Grid item xs={8}>
              <Typography className={classes.itemTitle}>
                {item.title}
              </Typography>
              <Typography className={classes.itemSubtitle}>
                {item.description}
              </Typography>
            </Grid>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Feature;
