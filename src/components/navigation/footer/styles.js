import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  footer: {},
  footerItems: {
    padding: '20px 0',
    textAlign: 'center'
  },
  img: {
    marginTop: 4
  },
  link: {
    color: theme.palette.common.black,
    textDecoration: 'none',
    '&:hover': {
      color: theme.palette.secondary.main,
      textDecoration: 'none'
    }
  },
  logo: {
    textAlign: 'center',
    alignSelf: 'center'
  },
  subtitle: {
    fontSize: '0.99rem',
    fontWeight: 700,
    marginBottom: 30,
    textAlign: 'center'
  }
}));

export default useStyles;
