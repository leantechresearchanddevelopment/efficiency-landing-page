// @packages
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Link from '../../general-purpose/ctrl-link';
import React from 'react';

// @styles
import useStyles from './styles';

// @constants
import footer from '../../../config/constants/footer';

// @images
import Logo from '../../../assets/images/lean-logo.png';

const Footer = () => {
  const classes = useStyles();

  return (
    <Container fixed>
      <Grid className={classes.footer} container justify="center">
        <Grid className={classes.logo} item md={2} xs={12}>
          <img className={classes.img} src={Logo} alt="lean-logo" />
        </Grid>
        {footer.map((item) => (
          <Grid
            className={classes.footerItems}
            item
            key={item.id}
            md={2}
            xs={12}
          >
            <Link
              className={classes.link}
              to={item.url}
            >
              {item.description}
            </Link>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Footer;
