// @packages
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Container from '@material-ui/core/Container';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import Grid from '@material-ui/core/Grid';
import Img from 'gatsby-image';
import MenuItem from '@material-ui/core/MenuItem';
import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import numeral from 'numeral';
import { navigate } from 'gatsby';
import { useStaticQuery, graphql } from 'gatsby';

// @scripts
import CtrlSearch from '../../general-purpose/ctrl-search';
import theme from '../../../theme';

// @styles
import useStyles from './styles';

// @query
const STORE = graphql`
  query {
    allDatoCmsBotsStore {
      nodes {
        botType
        id
        image {
          fluid(maxWidth: 300) {
            ...GatsbyDatoCmsFluid
          }
        }
        price
        slug
        subtitle
        title
      }
    }
  }
`;

const Store = () => {
  const classes = useStyles();
  const {
    allDatoCmsBotsStore: { nodes }
  } = useStaticQuery(STORE);
  const [search, setSearch] = useState('');
  const [filterData, setFilterData] = useState(nodes);

  const getColor = (type) => {
    switch (type) {
      case 'Load matching':
        return theme.palette.primary.main;
      case 'Manual Tracking':
        return theme.palette.secondary.main;
      case 'Email order to TMS data entry':
        return '#100050';
      default:
        break;
    }
  };

  const handleonSearch = (event) => {
    setSearch(event.target.value)

    setFilterData(nodes.filter(item => {
      return item.title.toString()
        .toLowerCase().includes(event.target.value
          .toLowerCase())
    }))
  };

  const handleOnNavigate = (page) => {
    navigate(`/${page}`);
  };

  return (
    <div>
      <div className={classes.storeHeader}>
        <Typography className={classes.storeTypography} variant='h3'>
          Bots store
        </Typography>
        <Typography className={classes.storeTypography}>
          Aenean sollicitudin, nec sagittis sem lorem quist bibe dum auctor.
        </Typography>
      </div>
      <Container style={{ overflow: "hidden", minHeight: '100vh' }}>
        <Grid className={classes.store} container spacing={3}>
          <Grid className={classes.options} item xs={12}>
            <CtrlSearch id="store" onSearch={handleonSearch} value={search} />
            <div className={classes.filter}>
              <Typography className={classes.filterText} display="inline">
                Filter by:
              </Typography>
              <TextField
                className={classes.select}
                id="outlined-select-currency"
                label=""
                select
                defaultValue="feature"
                variant="outlined"
              >
                <MenuItem value="feature">
                    Feature
                  </MenuItem>
              </TextField>
            </div>
          </Grid>
          {filterData.map(node => (
            <Grid item key={node.id} sm={4} xs={12}>
              <Card  onClick={() => handleOnNavigate(node.slug)}>
                <CardActionArea>
                  <div
                    className={classes.cardHeader}
                    style={{
                      backgroundColor: `${getColor(node.botType)}0D`,
                      borderColor: getColor(node.botType)
                    }}
                  >
                    <Typography
                      className={classes.cardHeaderTitle}
                      style={{ color: getColor(node.botType) }}
                    >
                      {node.botType}
                    </Typography>
                  </div>
                  <Img className={classes.image} fluid={node.image.fluid} />
                  <CardContent>
                    <div className={classes.cardHeaderContent}>
                      <Typography variant="h5" component="h2">
                        {node.title}
                      </Typography>
                      <Typography color="inherit" gutterBottom>
                        {numeral(node.price).format('($0.00)')}
                      </Typography>
                    </div>
                    <Typography variant="body2" color="textSecondary" component="p">
                      {node.subtitle}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          ))}
          {filterData.length <= 0 &&
            <div className={classes.alert}>
              <ErrorOutlineIcon className={classes.alertIcon} />
              <Typography variant="h4">
                No data
              </Typography>
            </div>
          }
        </Grid>
      </Container>
    </div>
  );
};

export default Store;
