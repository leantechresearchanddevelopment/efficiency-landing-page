// @packages
import { makeStyles } from '@material-ui/core/styles';

// @imagwa
import Background from '../../../assets/images/background.png';

const useStyles = makeStyles((theme) => ({
  alert: {
    alignItems: 'center',
    display: 'flex',
    height: '60vh',
    justifyContent: 'center',
    textAlign: 'center',
    width: '100%'
  },
  alertIcon: {
    color: 'orange',
    fontSize: '5rem'
  },
  cardHeader: {
    borderTop: '5px solid',
    height: 50
  },
  cardHeaderContent: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  cardHeaderTitle: {
    padding: 10
  },
  filter: {
    alignItems: 'center',
    display: 'flex'
  },
  filterText: {
    paddingRight: 5
  },
  header: {
    paddingBottom: 35
  },
  options: {
    display: 'flex',
    justifyContent: 'space-between',
    [theme.breakpoints.down('sm')]: {
      display: 'block'
    }
  },
  select: {
    width: 150,
    '& .MuiOutlinedInput-input': {
      padding: '7px 14px'
    }
  },
  store: {
    marginBottom: 20,
    marginTop: 15
  },
  storeHeader: {
    backgroundImage: `url(${Background})`,
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    fontSize: 16,
    padding: '110px 0 70px',
    position: 'relative',
    textAlign: 'center'
  },
  storeTypography: {
    color: theme.palette.common.white
  },
  subtitleTwo: {
    lineHeight: 1.75
  }
}));

export default useStyles;
