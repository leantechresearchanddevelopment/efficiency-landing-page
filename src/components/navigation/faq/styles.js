// @packages
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  accordion: {
    backgroundColor: `${theme.palette.secondary.main}01`,
    boxShadow: 'none',
    textAlign: 'initial'
  },
  accordionContent: {
    margin: '0 150px',
    [theme.breakpoints.down('sm')]: {
      margin: 0
    }
  },
  accordionDetails: {
    textAlign: 'initial'
  },
  bottomContent: {
    padding: '100px 300px',
    paddingBottom: 0,
    [theme.breakpoints.down('sm')]: {
      padding: '100px 0'
    },
    [theme.breakpoints.down('400')]: {
      paddingBottom: 0
    }
  },
  bottomContentButton: {
    color: theme.palette.common.white
  },
  button: {
    marginTop: 30,
    width: 180
  },
  container: {
    padding: '100px 0',
    textAlign: 'center'
  },
  faqView: {
    backgroundColor: `${theme.palette.secondary.main}0D`,
    margin: '40px 0'
  },
  header: {
    paddingBottom: 35
  },
  subtitle: {
    fontSize: '1.6rem',
    fontWeight: 600,
    marginBottom: 50,
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.5rem'
    }
  },
  subtitleTwo: {
    lineHeight: 1.75
  },
  title: {
    color: theme.palette.secondary.main,
    fontWeight: 600
  },
  titleTwo: {
    fontWeight: 600,
    lineHeight: 1.5,
    letterSpacing: '-0.025em',
    marginBottom: 30,
    [theme.breakpoints.down('md')]: {
      fontSize: '2.9rem'
    }
  }
}));

export default useStyles;
