// @packages
import MuiAccordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import React, { useState } from 'react';
import RemoveIcon from '@material-ui/icons/Remove';
import Typography from '@material-ui/core/Typography';
import { motion } from "framer-motion";
import { useStaticQuery, graphql } from 'gatsby';
import { withStyles } from '@material-ui/core/styles';

// @styles
import useStyles from './styles';

// @query
const FAQ = graphql`
  query {
    datoCmsFaq {
      buttonLoginWithEmail
      buttonWorkHistory
      description
      descriptionTwo
      items
      title
      titleTwo
    }
  }
`;

const Accordion = withStyles({
  root: {
    '&$expanded': {
      margin: 'auto'
    }
  },
  expanded: {}
})(MuiAccordion);

const Working = () => {
  const classes = useStyles();
  const [expanded, setExpanded] = useState('');
  const {
    datoCmsFaq: {
      buttonLoginWithEmail,
      buttonWorkHistory,
      description,
      descriptionTwo,
      items,
      title,
      titleTwo
    }
  } = useStaticQuery(FAQ);

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <div className={classes.faqView}>
      <Container fixed>
        <Grid className={classes.container} container id="faq">
          <Grid className={classes.header} item xs={12}>
            <Typography className={classes.title}>{title}</Typography>
            <Typography className={classes.subtitle}>{description}</Typography>
          </Grid>
          <Grid className={classes.accordionContent} item xs={12}>
            {JSON.parse(items).map((item, index, { length }) => (
              <React.Fragment key={item.id}>
                <Accordion
                  className={classes.accordion}
                  expanded={expanded === item.id}
                  onChange={handleChange(item.id)}
                >
                  <AccordionSummary
                    expandIcon={
                      expanded === item.id ? <RemoveIcon /> : <AddIcon />
                    }
                  >
                    <Typography className={classes.heading}>
                      {item.title}
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                  {expanded === item.id &&
                    <motion.section
                      key="content"
                      initial="collapsed"
                      animate="open"
                      exit="collapsed"
                    >
                      <motion.div
                        variants={{ collapsed: { opacity: 0 }, open: { opacity: 1 } }}
                        transition={{ duration: 1 }}
                        className="content-placeholder"
                      >
                        <Typography
                          className={classes.accordionDetails}
                          variant="caption"
                        >
                          {item.description}
                        </Typography>
                      </motion.div>
                    </motion.section>
                  }
                  </AccordionDetails>
                </Accordion>
                {index + 1 === length && <Divider />}
              </React.Fragment>
            ))}
            <Grid item xs={12}>
              <Button
                className={classes.button}
                variant="outlined"
                color="secondary"
              >
                Explore forums
              </Button>
            </Grid>
          </Grid>
          <Grid className={classes.bottomContent} container>
            <Grid item xs={12}>
              <Typography className={classes.titleTwo} variant="h3">
                {titleTwo}
              </Typography>
              <Typography className={classes.subtitleTwo}>
                {descriptionTwo}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Button
                className={`${classes.button} ${classes.bottomContentButton}`}
                variant="contained"
                color="secondary"
              >
                {buttonWorkHistory}
              </Button>
              <Button className={classes.button} color="secondary">
                {buttonLoginWithEmail}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Working;
