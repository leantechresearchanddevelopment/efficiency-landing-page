exports.createPages = async ({ actions, graphql, reporter }) => {
  const result = await graphql(`
    query {
      allDatoCmsBotsStore {
        nodes {
          slug
        }
      }
    }
  `);

  if (result.errors) {
    reporter.panic(`There was not results: ${result.errors}`);
  }

  const hasResults = result.data.allDatoCmsBotsStore.nodes;

  hasResults.forEach(element => {
    actions.createPage({
      path: element.slug,
      component: require.resolve('./src/components/navigation/store-detail/index.js'),
      context: {
        slug: element.slug
      }
    })
  });
}